package com.cn.liukaku.web.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.cn.liukaku")
public class WebAdminTwFeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebAdminTwFeignApplication.class, args);
    }
}
