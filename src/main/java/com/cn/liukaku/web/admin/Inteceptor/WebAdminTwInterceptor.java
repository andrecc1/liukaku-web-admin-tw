package com.cn.liukaku.web.admin.Inteceptor;

import com.cn.liukaku.common.domain.LiukakuUser;
import com.cn.liukaku.common.utils.CookieUtils;
import com.cn.liukaku.common.utils.MapperUtils;
import com.cn.liukaku.web.admin.service.RedisService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class WebAdminTwInterceptor implements HandlerInterceptor {
    @Resource
    private RedisService redisService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = CookieUtils.getCookieValue(request, "token");

        if (!StringUtils.isNotBlank(token)) {
            response.sendRedirect("http://localhost:8503/login?url=http://localhost:8504");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HttpSession session = request.getSession();
        LiukakuUser liukakuUser = (LiukakuUser) session.getAttribute("liukakuUser");
        if (liukakuUser != null) {
            if (modelAndView != null) {
                modelAndView.addObject("liukakuUser", liukakuUser);
            }
        } else {
            String token = CookieUtils.getCookieValue(request, "token");
            if(StringUtils.isNotBlank(token)){
                String loginCode=redisService.get(token);
                if(StringUtils.isNotBlank(loginCode)){
                    String json=redisService.get(loginCode);
                    if(StringUtils.isNotBlank(json)){
                        liukakuUser= MapperUtils.json2pojo(json,LiukakuUser.class);
                        if (modelAndView != null) {
                            modelAndView.addObject("liukakuUser", liukakuUser);
                        }
                        request.getSession().setAttribute("liukakuUser",liukakuUser);                   }
                }
            }
            redisService.get(token);
        }

        if(liukakuUser==null){
            response.sendRedirect("http://localhost:8503/login?url=http://localhost:8601");
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
