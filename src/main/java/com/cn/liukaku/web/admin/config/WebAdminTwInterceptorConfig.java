package com.cn.liukaku.web.admin.config;

import com.cn.liukaku.web.admin.Inteceptor.WebAdminTwInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebAdminTwInterceptorConfig implements WebMvcConfigurer {

    @Bean
    WebAdminTwInterceptor webAdminTwInterceptor(){
        return new WebAdminTwInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(webAdminTwInterceptor())
                .addPathPatterns("/**")
                .addPathPatterns("/static");

    }
}
