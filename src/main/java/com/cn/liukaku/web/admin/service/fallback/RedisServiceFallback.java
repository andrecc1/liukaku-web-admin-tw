package com.cn.liukaku.web.admin.service.fallback;

import com.cn.liukaku.web.admin.service.RedisService;
import org.springframework.stereotype.Component;


@Component
public class RedisServiceFallback implements RedisService {

    @Override
    public String put(String key, String value, long seconds) {

        //20200626之前
        //return Fallback.badGateWay();
        return null;
    }

    @Override
    public String get(String key) {
        //20200626之前
        //return Fallback.badGateWay();

        return null;
    }
}
